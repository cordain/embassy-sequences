#![no_std]
#![no_main]

extern crate alloc;

use embassy_sync::{blocking_mutex::raw::ThreadModeRawMutex, mutex::Mutex};

use alloc::sync::Arc;
use defmt::*;
use embassy_executor::Spawner;
use embassy_stm32::{bind_interrupts, dma::NoDma, peripherals, spi::{self, Config, Spi}, time::Hertz, usart::{self, BasicInstance, Uart}};
use embassy_time::Timer;
use {defmt_rtt as _, panic_probe as _};

bind_interrupts!(struct Irqs{
    USART2 => usart::InterruptHandler<peripherals::USART2>;
});

#[embassy_executor::task]
async fn uart_print(mut serial: Uart<'static, impl BasicInstance>, read_buff: Arc<Mutex<ThreadModeRawMutex,[u8;80]>>) {
    loop {
        let data = *read_buff.lock().await;
        serial.blocking_write(&data).unwrap();
        Timer::after_millis(10).await;
    }
}

#[embassy_executor::main]
async fn main(spawner: Spawner) {
    let p = embassy_stm32::init(Default::default());
    info!("Hello World!");

    let spi_config = spi::Config::default();
    spi_config.frequency = Hertz(1_000_000);

    let uart_config = usart::Config::default();
    uart_config.baudrate = 115_200;

    //let mut led = Output::new(p.PB3, Level::High, Speed::Low);
    let mut spi = Spi::new(p.SPI1, p.PB3, p.PB5, p.PB4, NoDma, NoDma, spi_config);
    let mut serial = Uart::new(p.USART2, p.PA15, p.PA2, Irqs, NoDma, NoDma, uart_config).unwrap();

    let mut print_buffer = Arc::new(Mutex::new([0u8;80]));

    unwrap!(spawner.spawn(uart_print(serial, Arc::clone(&print_buffer))));

    loop {
        led.set_high();
        Timer::after_millis(500).await;
        led.set_low();
        Timer::after_millis(500).await;
    }
}
